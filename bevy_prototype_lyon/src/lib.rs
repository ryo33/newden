// Copyright (c) 2020 Federico Rinaldi

// Permission is hereby granted, free of charge, to any
// person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the
// Software without restriction, including without
// limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software
// is furnished to do so, subject to the following
// conditions:

// The above copyright notice and this permission notice
// shall be included in all copies or substantial portions
// of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
// ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
// SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

use bevy::{
    prelude::*,
    render::mesh::{Indices, VertexAttributeValues},
};
use lyon::{
    lyon_tessellation::{BuffersBuilder, StrokeAttributes, StrokeOptions, StrokeTessellator},
    tessellation::VertexBuffers,
};
use lyon_path::Path;

pub struct LyonPath {
    pub path: Path,
    pub options: StrokeOptions,
}

// TODO: Replace this by bevy_prototype_lyon or bevy_prototype_lyon.
impl From<LyonPath> for Mesh {
    fn from(mesh: LyonPath) -> Self {
        let mut tessellator = StrokeTessellator::new();
        let mut vertice = VertexBuffers::new();

        tessellator
            .tessellate_path(
                mesh.path.as_slice(),
                &mesh.options,
                &mut BuffersBuilder::new(
                    &mut vertice,
                    |pos: lyon::math::Point, _: StrokeAttributes| [pos.x, pos.y, 0.0],
                ),
            )
            .unwrap();
        let vertices = vertice.vertices;
        let indices = vertice.indices;

        let mut mesh = Self::new(bevy::render::pipeline::PrimitiveTopology::TriangleList);
        mesh.set_attribute(
            Mesh::ATTRIBUTE_POSITION,
            VertexAttributeValues::from(vertices),
        );
        mesh.set_indices(Some(Indices::U32(indices)));

        mesh
    }
}
