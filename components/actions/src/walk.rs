pub enum Direction {
    Left,
    Right,
}

pub struct WalkSpeed(pub f32);

pub enum WalkState {
    Idle,
    Walking(Direction),
}

impl WalkState {
    pub fn is_idle(&self) -> bool {
        match self {
            Self::Idle => true,
            _ => false,
        }
    }
    pub fn is_walking(&self) -> bool {
        match self {
            Self::Walking(_) => true,
            _ => false,
        }
    }
}