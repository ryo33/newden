use nden_core::bezier::{into_bevy_vec2, into_vek_vec2, split_curve, Bezier, CubicBezier2};
use nden_core::*;

pub const GROUND_THICKNESS: Meter = 0.4;

#[derive(Default)]
pub struct Animation {
    pub speed: f32,
    pub diff: f32,
}

pub struct OnSlope {
    pub is: Entity,
    pub on: Entity,
}

fn create_bezier_from_two_slope_points(start_point: &SlopePoint, end_point: &SlopePoint) -> Bezier {
    use nden_core::bezier::Vek2;
    let start = into_vek_vec2(&start_point.position);
    let end = into_vek_vec2(&end_point.position);
    let curve = CubicBezier2 {
        start,
        ctrl0: start + Vek2::new(start_point.right_length, 0.0).rotated_z(start_point.right_theta),
        ctrl1: end + Vek2::new(end_point.left_length, 0.0).rotated_z(end_point.left_theta),
        end,
    };
    let mut splitted = vec![];
    split_curve(&mut splitted, curve);
    Bezier {
        curve: curve.clone(),
        splitted,
    }
}

#[derive(Bundle)]
pub struct SlopeComponents {
    slope: Slope,
    slope_curves: SlopeCurves,
    transform: Transform,
    global_transform: GlobalTransform,
}

#[derive(Default)]
pub struct SlopePos {
    pub x: f32,
    pub y: f32,
}

impl SlopePos {
    pub fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }
}

pub struct Slope;

#[derive(Clone)]
pub struct SlopeCurves {
    pub changed_indices: Vec<bool>, // len() must be indices.len()
    pub points: Vec<SlopePoint>,    // len() must be indices.len()
    pub curves: Vec<Bezier>,        // len() must be indices.len() - 1
}

impl Default for SlopeCurves {
    fn default() -> Self {
        Self::new(vec![SlopePoint::default(), SlopePoint::default()])
    }
}

#[derive(Clone, Default)]
pub struct SlopePoint {
    pub position: Vec2,
    pub left_theta: f32,
    pub left_length: f32,
    pub right_theta: f32,
    pub right_length: f32,
}

impl SlopeCurves {
    pub fn new(slope_points: Vec<SlopePoint>) -> Self {
        let mut points = Vec::with_capacity(slope_points.len());
        for point in slope_points.into_iter() {
            points.push(point.clone());
        }

        let curves = vec![Bezier::default(); points.len() - 1];
        let changed_indices = vec![true; points.len()];

        let mut slope = Self {
            changed_indices,
            points,
            curves,
        };
        slope.update_curves();
        slope
    }

    pub fn update_curves(&mut self) {
        for (idx, changed) in self
            .changed_indices
            .windows(2)
            .map(|w| w[0] || w[1])
            .enumerate()
        {
            if changed {
                self.curves[idx] =
                    create_bezier_from_two_slope_points(&self.points[idx], &self.points[idx + 1]);
            }
        }
    }

    pub fn get_position(&self, pos: f32) -> Vec2 {
        let mut len = 0.;
        for curve in self.curves.iter() {
            for line in curve.splitted.iter() {
                let line_len = line.start.distance(line.end);
                let start = line.start;
                let end = line.end;
                if pos <= len + line_len {
                    return into_bevy_vec2(&(start + (end - start) * (pos - len) / line_len));
                }
                len += line_len;
            }
        }
        let line = self.curves.last().unwrap().splitted.last().unwrap();
        let line_len = line.start.distance(line.end);
        let start = line.start;
        let end = line.end;
        into_bevy_vec2(&(start + (end - start) * (pos + line_len - len) / line_len))
    }
}
