use nden_core::*;

pub struct FOV {
    pub height: Meter,
    pub width: Meter,
}

pub const RENDER_WIDTH: Meter = 32.0;
pub const RENDER_HEIGHT: Meter = 18.0;
