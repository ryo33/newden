use bevy::prelude::*;
pub use vek::bezier::repr_c::CubicBezier2;
pub use vek::vec::repr_c::Vec2 as Vek2;

#[derive(Clone, Default)]
pub struct Bezier {
    pub curve: CubicBezier2<f32>,
    pub splitted: Vec<CubicBezier2<f32>>,
}

pub fn into_vek_vec2(from: &Vec2) -> vek::vec::repr_c::Vec2<f32> {
    vek::vec::repr_c::Vec2 {
        x: from.x(),
        y: from.y(),
    }
}

pub fn into_bevy_vec2(from: &vek::vec::repr_c::Vec2<f32>) -> Vec2 {
    Vec2::new(from.x, from.y)
}

const EPSILON: f32 = 0.07;
pub fn split_curve(base_vec: &mut Vec<CubicBezier2<f32>>, curve: CubicBezier2<f32>) {
    let line_seg_len_pow2 =
        (curve.end.x - curve.start.x).powi(2) + (curve.end.y - curve.start.y).powi(2);
    let curve_len = curve.length_by_discretization(5);
    if curve_len.powi(2) - line_seg_len_pow2 > EPSILON {
        let splitted = curve.split(0.5);
        split_curve(base_vec, splitted[0]);
        split_curve(base_vec, splitted[1]);
    } else {
        base_vec.push(curve);
    }
}
