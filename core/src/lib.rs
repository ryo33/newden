pub mod bezier;

pub use bevy;
pub use bevy::prelude::*;
pub use bevy_rapier2d;

pub type Meter = f32;
