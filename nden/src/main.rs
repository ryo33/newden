use nden_core::*;
use nden_plugins::{
    DiagnosticsPlugin, InputPlugin, PedestrianPlugin, RapierIntegrationPlugin, ScenePlugin,
    SlopePlugin, ViewportPlugin,
};

fn main() {
    App::build()
        // .add_resource(Msaa { samples: 8 })
        .add_resource(WindowDescriptor {
            title: "New Den".into(),
            ..Default::default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(RapierIntegrationPlugin)
        .add_plugin(ScenePlugin)
        .add_plugin(InputPlugin)
        .add_plugin(ViewportPlugin)
        .add_plugin(SlopePlugin)
        .add_plugin(PedestrianPlugin)
        .add_plugin(DiagnosticsPlugin)
        .add_system(bevy::input::system::exit_on_esc_system.system())
        .run();
}
