use nden_core::bevy::{
    diagnostic::Diagnostics, diagnostic::FrameTimeDiagnosticsPlugin, input::Input,
    prelude::KeyCode, prelude::Res,
};
use nden_core::*;

pub fn diagnostics(diagnostics: Res<Diagnostics>, keyboard_input: Res<Input<KeyCode>>) {
    if keyboard_input.pressed(KeyCode::F3) {
        if let Some(fps) = diagnostics.get(FrameTimeDiagnosticsPlugin::FPS) {
            if let Some(average) = fps.average() {
                println!("FPS: {:.2}", average);
            }
        }
    }
}
pub struct DiagnosticsPlugin;

impl Plugin for DiagnosticsPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_plugin(FrameTimeDiagnosticsPlugin::default())
            .add_system(diagnostics.system());
    }
}
