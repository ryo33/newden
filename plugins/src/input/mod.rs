use nden_actions::walk::{Direction::*, WalkState};
use nden_core::*;
use nden_pedestrian::Yayaka;
use nden_slope::SlopePos;
pub struct InputPlugin;

impl Plugin for InputPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_system(input.system());
    }
}

fn input(
    time: Res<Time>,
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<(&Yayaka, &mut SlopePos, &mut WalkState)>,
) {
    for (_yayaka, mut slope_pos, mut walk_state) in query.iter_mut() {
        let mut direction = 0.0;
        if keyboard_input.pressed(KeyCode::Left) {
            direction -= 1.0;
            *walk_state = WalkState::Walking(Left);
        } else if keyboard_input.pressed(KeyCode::Right) {
            direction += 1.0;
            *walk_state = WalkState::Walking(Right);
        } else {
            *walk_state = WalkState::Idle;
        }

        slope_pos.x += time.delta_seconds * direction * 5.0;
    }
}
