use nden_core::*;

use self::{
    rendering::{animate_sprite, flip_sprite},
    updating::{freeze_rotation, walk},
};

mod rendering;
mod updating;

pub struct PedestrianPlugin;

impl Plugin for PedestrianPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_system(animate_sprite.system())
            .add_system_to_stage(stage::POST_UPDATE, flip_sprite.system())
            .add_system(walk.system())
            .add_system(freeze_rotation.system());
    }
}
