use nden_actions::walk::{Direction, WalkState};
use nden_core::*;

pub fn animate_sprite(
    query: Query<(&Timer, &WalkState, &Children)>,
    mut sprite_query: Query<&mut TextureAtlasSprite>,
) {
    for (timer, walk_state, children) in query.iter() {
        if timer.finished {
            for &entity in children.iter() {
                if let Ok(mut sprite) = sprite_query.get_mut(entity) {
                    if walk_state.is_idle() && (sprite.index == 37 || sprite.index == 41) {
                        sprite.index = 0;
                    }
                    if sprite.index != 0 {
                        sprite.index = 36 + (sprite.index - 4 + 1) % 8;
                    }
                    if walk_state.is_walking() && sprite.index == 0 {
                        sprite.index = 38;
                    }
                }
            }
        }
    }
}

pub fn flip_sprite(
    time: Res<Time>,
    mut query: Query<(Changed<WalkState>, &Children)>,
    mut transform: Query<With<TextureAtlasSprite, &mut Transform>>,
) {
    for (walk_state, children) in query.iter_mut() {
        for &entity in children.iter() {
            if let Ok(mut transform) = transform.get_mut(entity) {
                match &*walk_state {
                    WalkState::Walking(Direction::Left) => {
                        transform.rotation = Quat::from_rotation_y(std::f32::consts::PI)
                    }
                    WalkState::Walking(Direction::Right) => {
                        transform.rotation = Quat::from_rotation_y(0.0)
                    }
                    WalkState::Idle => {
                        if time.seconds_since_startup as u32 % 2 == 0 {
                            transform.rotation = Quat::from_rotation_y(0.0)
                        } else {
                            transform.rotation = Quat::from_rotation_y(std::f32::consts::PI)
                        }
                    }
                }
            }
        }
    }
}
