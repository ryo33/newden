use nden_actions::walk::{Direction, WalkSpeed, WalkState};
use nden_core::bevy_rapier2d::{
    na::Vector2, physics::RigidBodyHandleComponent, rapier::dynamics::RigidBodySet,
};
use nden_core::*;

pub(crate) fn freeze_rotation(
    mut bodies: ResMut<RigidBodySet>,
    query: Query<With<WalkState, Added<RigidBodyHandleComponent>>>,
) {
    for body_handle in query.iter() {
        if let Some(mut body) = bodies.get_mut(body_handle.handle()) {
            body.mass_properties.inv_principal_inertia_sqrt = 0.0;
        }
    }
}

pub(crate) fn walk(
    time: Res<Time>,
    mut bodies: ResMut<RigidBodySet>,
    query: Query<(&WalkState, &WalkSpeed, &RigidBodyHandleComponent)>,
) {
    for (walk_state, walk_speed, body_handle) in query.iter() {
        let walk_dir = match walk_state {
            WalkState::Walking(Direction::Left) => -1.,
            WalkState::Walking(Direction::Right) => 1.,
            WalkState::Idle => return,
        };
        if let Some(mut body) = bodies.get_mut(body_handle.handle()) {
            let mass = body.mass();
            body.apply_force(Vector2::new(
                (walk_dir * walk_speed.0) / time.delta_seconds * mass,
                0.0,
            ));
            body.wake_up(true)
        }
    }
}
