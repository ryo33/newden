use nden_core::bevy_rapier2d::{
    na::Vector,
    physics::{EventQueue, RapierConfiguration, RapierPhysicsPlugin},
    rapier::dynamics::IntegrationParameters,
    render::RapierRenderPlugin,
};
use nden_core::*;
pub struct RapierIntegrationPlugin;

impl Plugin for RapierIntegrationPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_plugin(RapierPhysicsPlugin)
            // .add_plugin(RapierRenderPlugin)
            .add_resource(RapierConfiguration {
                gravity: Vector::y() * -9.8,
                scale: 1.0,
                physics_pipeline_active: true,
                query_pipeline_active: true,
                ..Default::default()
            })
            .add_startup_system(tweak_integration_parameters.system())
            .add_system(collision.system())
            .add_resource(EventQueue::new(false));
    }
}

fn tweak_integration_parameters(mut _integration_parameters: ResMut<IntegrationParameters>) {}

fn collision(events: Res<EventQueue>) {
    while let Ok(proximity_event) = events.proximity_events.pop() {
        println!("Received proximity event: {:?}", proximity_event);
    }

    while let Ok(contact_event) = events.contact_events.pop() {
        println!("Received contact event: {:?}", contact_event);
    }
}
