use bevy_rapier2d::{
    na::{Point2, Point3},
    rapier::{dynamics::RigidBodyBuilder, geometry::ColliderBuilder},
};
use nden_actions::walk::{WalkSpeed, WalkState};
use nden_core::*;
use nden_pedestrian::Yayaka;
use nden_slope::{Animation, OnSlope, Slope, SlopeCurves, SlopePoint, SlopePos, GROUND_THICKNESS};

pub struct ScenePlugin;

impl Plugin for ScenePlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(setup.system());
    }
}

fn setup(
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    asset_server: Res<AssetServer>,
) {
    let yayaka_texture_handle = asset_server.load("textures/character1.png");
    let yayaka_texture_atlas =
        TextureAtlas::from_grid(yayaka_texture_handle, Vec2::new(96., 148.), 9, 5);
    let yayaka_texture_atlas_handle = texture_atlases.add(yayaka_texture_atlas);

    let yayaka = commands
        .spawn((
            RigidBodyBuilder::new_dynamic().translation(0.0, 5.0),
            ColliderBuilder::ball(0.85),
            Transform::default(),
            GlobalTransform::default(),
        ))
        .with(Yayaka)
        .with(SlopePos::new(0., 1.))
        .with(WalkState::Idle)
        .with(WalkSpeed(0.7))
        .with(Timer::from_seconds(0.05, true))
        .with_children(|yayaka| {
            yayaka.spawn(SpriteSheetComponents {
                texture_atlas: yayaka_texture_atlas_handle,
                transform: Transform::from_scale(Vec3::one() * 2.0 / 148.),
                sprite: TextureAtlasSprite::new(0),
                ..Default::default()
            });
        })
        .current_entity()
        .unwrap();
    let slope = commands
        .spawn((
            Slope,
            SlopeCurves::default(),
            GlobalTransform::default(),
            Transform::default(),
        ))
        .with(Timer::from_seconds(0.2, true))
        .with_bundle(SpriteComponents {
            mesh: meshes.add(Mesh::new(
                bevy::render::pipeline::PrimitiveTopology::TriangleList,
            )),
            material: materials.add(ColorMaterial::color(Color::rgba(0., 1., 0., 0.1))),
            sprite: Sprite {
                size: Vec2::new(1., 1.),
                ..Default::default()
            },
            ..Default::default()
        })
        .with_children(|slope| {
            slope
                .spawn((
                    SlopePoint {
                        position: Vec2::new(-14., 4.),
                        right_theta: 30_f32.to_radians(),
                        right_length: 10.,
                        ..Default::default()
                    },
                    GlobalTransform::default(),
                    Transform::default(),
                ))
                .spawn((
                    SlopePoint {
                        position: Vec2::new(-2., 1.),
                        left_theta: (180.0 + 10.0_f32).to_radians(),
                        left_length: 5.,
                        right_theta: 10.0_f32.to_radians(),
                        right_length: 4.,
                        ..Default::default()
                    },
                    GlobalTransform::default(),
                    Transform::default(),
                ))
                .with(Animation {
                    speed: 1.3,
                    ..Default::default()
                })
                .spawn((
                    SlopePoint {
                        position: Vec2::new(7., 0.),
                        left_theta: (180.0 - 20.0_f32).to_radians(),
                        left_length: 6.,
                        right_theta: -20.0_f32.to_radians(),
                        right_length: 4.,
                        ..Default::default()
                    },
                    GlobalTransform::default(),
                    Transform::default(),
                ))
                .with(Animation {
                    speed: 1.,
                    ..Default::default()
                })
                .spawn((
                    SlopePoint {
                        position: Vec2::new(17., 0.),
                        left_theta: (180.0 + 45.0_f32).to_radians(),
                        left_length: 10.,
                        ..Default::default()
                    },
                    GlobalTransform::default(),
                    Transform::default(),
                ));
        })
        .current_entity()
        .unwrap();
    commands.spawn((
        OnSlope {
            is: yayaka,
            on: slope,
        },
        RigidBodyBuilder::new_kinematic(),
        ColliderBuilder::trimesh(
            vec![
                Point2::new(-3.5, GROUND_THICKNESS / 2.),
                Point2::new(-3.5, -GROUND_THICKNESS / 2.),
                Point2::new(3.5, -GROUND_THICKNESS / 2.),
                Point2::new(3.5, GROUND_THICKNESS / 2.),
            ],
            vec![Point3::new(0, 1, 2), Point3::new(0, 2, 3)],
        ), // sames as ColliderBuilder::cuboid(7., GROUND_THICKNESS / 2.),
    ));
}
