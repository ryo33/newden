mod physics;
mod rendering;
mod updating;

use nden_core::*;

use self::{physics::slope_pos, rendering::render_slope, updating::{simple_animation, update_slope_curves}};

pub struct SlopePlugin;

impl Plugin for SlopePlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_system(update_slope_curves.system())
            .add_system(simple_animation.system())
            .add_system(slope_pos.system())
            .add_system(render_slope.system());
    }
}
