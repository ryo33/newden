use std::cmp::Ordering;

use nden_core::bevy_rapier2d::{
    na::{Isometry2, Vector2},
    physics::{ColliderHandleComponent, RigidBodyHandleComponent},
    rapier::{dynamics::RigidBodySet, geometry::ColliderSet},
};
use nden_core::bezier::Vek2;
use nden_core::*;
use nden_slope::{OnSlope, SlopeCurves};

pub fn slope_pos(
    mut bodies: ResMut<RigidBodySet>,
    collders: ResMut<ColliderSet>,
    slopes: Query<&SlopeCurves>,
    walkers: Query<&ColliderHandleComponent>,
    mut on_slope: Query<(&OnSlope, &RigidBodyHandleComponent)>,
) {
    for (on_slope, slope_handle) in on_slope.iter_mut() {
        let ball_handle = walkers.get(on_slope.is).unwrap();
        let slope = slopes
            .get(on_slope.on)
            .map_err(|e| println!("{:?}", e))
            .unwrap();
        let mut slope_body = bodies.get_mut(slope_handle.handle()).unwrap();
        let ball_collider = collders.get(ball_handle.handle()).unwrap();
        let ball_vek = Vek2::new(
            ball_collider.position().translation.x,
            ball_collider.position().translation.y,
        );
        let (idx, _) = slope
            .curves
            .iter()
            .enumerate()
            .map(|(idx, curve)| {
                let (t, _) = curve.curve.binary_search_point_by_steps(ball_vek, 5, 0.1);
                (idx, (t - 0.5).abs())
            })
            .min_by(|(_, first), (_, second)| first.partial_cmp(second).unwrap_or(Ordering::Equal))
            .unwrap();

        let (t, _) = slope.curves[idx]
            .curve
            .binary_search_point_by_steps(ball_vek, 100, 0.001);

        let curve = slope.curves[idx].curve;
        let position = curve.evaluate(t);
        let derivative = curve.evaluate_derivative(t);

        slope_body.set_position(Isometry2::new(
            Vector2::new(position.x, position.y),
            derivative.y.atan2(derivative.x),
        ));
        slope_body.wake_up(true);
    }
}
