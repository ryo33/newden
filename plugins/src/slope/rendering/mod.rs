use nden_core::*;

use bevy_prototype_lyon::LyonPath;
use lyon::{lyon_tessellation::{LineCap, LineJoin, StrokeOptions}, math::point};
use lyon_path::Path;
use nden_slope::{GROUND_THICKNESS, SlopeCurves};

pub fn render_slope(
    mut meshes: ResMut<Assets<Mesh>>,
    mut query: Query<(Changed<SlopeCurves>, &Handle<Mesh>)>,
) {
    for (slope, mesh) in query.iter_mut() {
        let mut path_builder = Path::builder();

        let start = slope.curves[0].curve.start;
        path_builder.move_to(point(start.x, start.y));
        for curves in slope.curves.windows(2) {
            let start = curves[0].curve;
            let end = curves[1].curve;
            path_builder.cubic_bezier_to(
                point(start.ctrl0.x, start.ctrl0.y),
                point(start.ctrl1.x, start.ctrl1.y),
                point(end.start.x, end.start.y),
            );
        }
        let end = slope.curves.last().unwrap().curve;
        path_builder.cubic_bezier_to(
            point(end.ctrl0.x, end.ctrl0.y),
            point(end.ctrl1.x, end.ctrl1.y),
            point(end.end.x, end.end.y),
        );
        let path = path_builder.build();
        let options = StrokeOptions::default()
            .with_tolerance(0.01)
            .with_line_width(GROUND_THICKNESS + 0.2)
            .with_line_cap(LineCap::Round)
            .with_line_join(LineJoin::Round);

        meshes.set(mesh, LyonPath { path, options }.into());
    }
}
