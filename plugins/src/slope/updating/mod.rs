use nden_core::*;

use nden_slope::{Animation, SlopeCurves, SlopePoint};

pub fn update_slope_curves(
    mut slopes: Query<(&mut SlopeCurves, &Children)>,
    slope_points: Query<&SlopePoint>,
) {
    for (mut curves, children) in slopes.iter_mut() {
        if children.len() != 0 {
            *curves = SlopeCurves::new(
                children
                    .iter()
                    .flat_map(|child| slope_points.get_component::<SlopePoint>(*child).into_iter())
                    .map(|slope_point| (*slope_point).clone())
                    .collect(),
            );
        }
    }
}

pub fn simple_animation(time: Res<Time>, mut query: Query<(&mut SlopePoint, &mut Animation)>) {
    for (mut point, mut animation) in query.iter_mut() {
        *point.position.y_mut() -= animation.diff;
        animation.diff = f32::sin(time.time_since_startup().as_secs_f32() * animation.speed * 2.);
        *point.position.y_mut() += animation.diff;
    }
}
