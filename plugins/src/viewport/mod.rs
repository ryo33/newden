use nden_core::bevy::render::camera::OrthographicProjection;
use nden_core::*;
use nden_viewport::{FOV, RENDER_HEIGHT, RENDER_WIDTH};

pub struct ViewportPlugin;

impl Plugin for ViewportPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(start.system())
            .add_system(update_rendering_region.system());
    }
}

fn start(mut commands: Commands) {
    commands
        .spawn((
            FOV {
                width: RENDER_WIDTH,
                height: RENDER_HEIGHT,
            },
            GlobalTransform::default(),
            Transform::default(),
        ))
        // cameras
        .spawn(Camera2dComponents::default())
        .spawn(UiCameraComponents::default());
}

fn update_rendering_region(
    windows: ChangedRes<Windows>,
    // mut query: Query<With<FOV, &mut Transform>>,
    mut camera: Query<(&OrthographicProjection, &mut Transform)>,
) {
    if let Some(window) = windows.get_primary() {
        let zoom = f32::min(
            window.width() as f32 / RENDER_WIDTH,
            window.height() as f32 / RENDER_HEIGHT,
        );
        let scale = 1. / zoom;
        for (_, mut transform) in camera.iter_mut() {
            transform.scale.set_x(scale);
            transform.scale.set_y(scale);
        }
    }
}
